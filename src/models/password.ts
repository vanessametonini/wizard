export interface Password {
  confirm: string;
  value: string;
}
