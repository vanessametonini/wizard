export interface WizardState {
  sysKey: string;
  skeletonKey: string;
  storage: string;
  sementeira: string[];
  netKey: string;
}
