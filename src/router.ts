import Vue from 'vue';
import Router from 'vue-router';
import SysKey from './views/SysKey.vue';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'step1',
      component: SysKey,
    },
    {
      path: '/step2',
      name: 'step2',
      component: () => import('./views/SkeletonKey.vue'),
    },
    {
      path: '/step3',
      name: 'step3',
      component: () => import('./views/Storage.vue'),
    },
    {
      path: '/step4',
      name: 'step4',
      component: () => import('./views/Sementeira.vue'),
    },
    {
      path: '/step5',
      name: 'step5',
      component: () => import('./views/NetKey.vue'),
    },
    {
      path: '/review',
      name: 'review',
      component: () => import('./views/Review.vue'),
    },
    {
      path: '/confirm',
      name: 'confirm',
      component: () => import('./views/Confirm.vue'),
    },
  ],
});
